package com.ikkong.system.controller.base;

import com.ikkong.core.jfinal.ext.shiro.RequiresUrlPermission;
import com.ikkong.core.base.BaseController;

@RequiresUrlPermission
public class AdminBaseController extends BaseController {

}
