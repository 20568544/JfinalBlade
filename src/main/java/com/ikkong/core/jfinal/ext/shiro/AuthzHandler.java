package com.ikkong.core.jfinal.ext.shiro;

import com.jfinal.aop.Invocation;
import org.apache.shiro.authz.AuthorizationException;

/**
 * 访问控制处理器接口
 * @author dafei
 *
 */
interface AuthzHandler {
	/**
	 * 访问控制检查
	 * @throws AuthorizationException 授权异常
	 */
	void assertAuthorized(Invocation ai)throws AuthorizationException;
}
