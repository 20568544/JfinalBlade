package com.ikkong.core.jfinal.ext.shiro;

import java.util.List;

import com.jfinal.aop.Invocation;
import org.apache.shiro.authz.AuthorizationException;

class CompositeAuthzHandler implements AuthzHandler {

	private final List<AuthzHandler> authzHandlers;

	public CompositeAuthzHandler(List<AuthzHandler> authzHandlers){
		this.authzHandlers = authzHandlers;
	}

	public void assertAuthorized(Invocation ai) throws AuthorizationException {
		for(AuthzHandler authzHandler : authzHandlers){
			authzHandler.assertAuthorized(ai);
		}
	}
}